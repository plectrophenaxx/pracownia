<?php

namespace Tests\Feature;

use App\Models\Review;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class reviewControllerTest extends TestCase{
    public function test_create(){
        $response = $this->postJson('/api/post', ["score" =>"5", "review" =>"test", "user_id" =>"1", "recipe_id" =>"1"]);

        $response->assertStatus(200);
    }
    public function test_read(){
        $response = $this->postJson('/api/read/'.Review::inRandomOrder()->first()->id);

        $response->assertStatus(200);
    }
    public function test_update(){
        $response = $this->postJson('/api/update/'.Review::inRandomOrder()->first()->id, ["name" =>"test"]);

        $response->assertStatus(200);
    }
    public function test_delete(){
        $response = $this->postJson('/api/delete/'.Review::inRandomOrder()->first()->id);

        $response->assertStatus(200);
    }
}
