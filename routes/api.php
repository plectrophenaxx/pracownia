<?php

use App\Http\Controllers\reviewController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('post', [reviewController::class, "create"]);
Route::post('read/{review}', [reviewController::class, "read"]);

Route::post('update/{review}', [reviewController::class, "update"]);
Route::post('delete/{review}', [reviewController::class, "delete"]);

Route::post('listreviews', [reviewController::class, "listReviews"]);

Route::post('eksport', [reviewController::class, "eksport"]);
