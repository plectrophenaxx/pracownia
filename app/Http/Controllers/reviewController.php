<?php

namespace App\Http\Controllers;

use App\Models\Recipe;
use App\Models\Review;
use App\Models\User;
use Illuminate\Http\Request;

class reviewController extends Controller{

    public function create(Request $request){
        Review::create($request->all());
        return 200;
    }
    public function read(Review $review){
        return $review->load(['user','recipe.user']);
    }

    public function update(Request $request, Review $review){
        $review->update($request->all());
        return $review;
    }
    public function delete(Review $review){
        $review->delete();
        return 200;
    }
    public function listReviews(Request $request){
        if (isset($request->recipe_id)){
            return Review::where("recipe_id", $request->recipe_id)->with(['user','recipe.user'])->get();
        }
        else{
            return Review::with(['user','recipe.user'])->get();
        }
    }
    public function eksport(Request $request){
        return ["review" => Review::all(), "recipe" => Recipe::all(), "user" => User::all()];
    }
}
